import java.util.Comparator;

public class Books {
	
	public static void main(String args[]) { System.out.println("My first program in Java, HelloWorld !!"); }

	public int bookId;
	public String bookName;
	public String authorName;
	public String category;
	public String publisher;
	public double price;

	public Books() {
		this.bookId = 111111;
		this.bookName = "";
		this.authorName = "";
		this.category = "";
		this.publisher = "";
		this.price = 0.0;

	}

	public Books(int bookId, String bookName, String authorName, String category, String publisher, double price) {
		super();
		this.bookId = bookId;
		this.bookName = bookName;
		this.authorName = authorName;
		this.category = category;
		this.publisher = publisher;
		this.price = price;
	}

	public int getBookId() {
		return bookId;
	}

	public void setBookId(int bookId) {
		this.bookId = bookId;
	}

	public String getBookName() {
		return bookName;
	}

	public void setBookName(String bookName) {
		this.bookName = bookName;
	}

	public String getAuthorName() {
		return authorName;
	}

	public void setAuthorName(String authorName) {
		this.authorName = authorName;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getPublisher() {
		return publisher;
	}

	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public static Comparator<Books>bkBookNameComparator =new Comparator<Books>() {
	public int compare(Books b1,Books b2){String bkBookName1=b1.getBookName().toUpperCase();String bkBookName2=b2.getBookName().toUpperCase();return bkBookName1.compareTo(bkBookName2);}

	};
	public static Comparator<Books>bkAuthorNameComparator = new Comparator<Books>() {
	public int compare(Books b1,Books b2){String bkAuthorName1=b1.getAuthorName().toUpperCase();String bkAuthorName2=b2.getAuthorName().toUpperCase();return bkAuthorName1.compareTo(bkAuthorName2);}

	};
	public static Comparator<Books>bkCategoryComparator = new Comparator<Books>() {
	public int compare(Books b1,Books b2){String bkCategory1=b1.getCategory().toUpperCase();String bkCategory2=b2.getCategory().toUpperCase();return bkCategory1.compareTo(bkCategory2);}

	};

public String toString() {
	return "Book Id: "+bookId+"\nBook Name: "+bookName +"\nAuthor Name: "+authorName +"\n Category: "+category +"\nPublisher: "+publisher +"\nPrice: "+price;
}
}


