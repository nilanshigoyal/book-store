import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class testBook {
	static ArrayList<Books> books;

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scan = new Scanner(System.in);
		books = new ArrayList<Books>();
		readFromFile("bk.txt");
		System.out.println("*****************Books Management System********************");
		int option = 0;
		do {
			menu();
			System.out.print("Please select option: ");
			option = scan.nextInt();
			switch (option) {
			case 1:
				displayInfo();
				break;
			case 2:
				displayInfoByBookName();
				break;
			case 3:
				displayInfoByAuthorName();
				break;
			case 4:
				displayInfoByCategory();
				break;
			case 5:
				searchByBookId();
				break;
			case 6:
				searchByAuthorName();
				break;
			case 7:
				searchByCategory();
				break;
			case 8:
				modifyBooksData();
				break;
			case 9:
				System.out.println("Thanks for using System, Bye!");
				break;
			default:
				System.out.println("Invalid Choice! Try again!");
				break;

			}
		} while (option != 9);
		writeToFile("bk.txt", books);

	}

	private static void readFromFile(String fileName) {
		File file = new File(fileName);
		try {
			Scanner sc = new Scanner(file);
			sc.nextLine();
			while (sc.hasNextLine()) {
				String line = sc.nextLine();
				int bkId = Integer.parseInt(line.split(",")[0]);
				String bookName = line.split(",")[1];
				String authorName = line.split(",")[2];
				String category = line.split(",")[3];
				String publisher = line.split(",")[4];
				double price = Double.parseDouble(line.split(",")[5]);
				books.add(new Books(bkId, bookName, authorName, category, publisher, price));

			}
			sc.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	private static void writeToFile(String fileName, ArrayList<Books> books) {
		try {
			FileWriter fw = new FileWriter(fileName);
			fw.write("BookId,BookName,AuthorName,Category,Publisher,Price\n");
			for (Books bks : books) {
				fw.write(bks.getBookId() + "," + bks.getBookName() + "," + bks.getAuthorName() + ","
						+ bks.getCategory() + "," + bks.getPublisher() + "," + bks.getPrice());
				fw.write("\n");
			}
			fw.flush();
			fw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private static void displayInfo() {
		for (Books bks : books) {
			System.out.println(bks.toString());
		}
	}

	private static void displayInfoByBookName() {
		Collections.sort(books, Books.bkBookNameComparator);
		for (Books bks : books) {
			System.out.println(bks.toString());
		}
	}

	private static void displayInfoByAuthorName() {
		Collections.sort(books, Books.bkAuthorNameComparator);
		for (Books bks : books) {
			System.out.println(bks.toString());
		}
	}

	private static void displayInfoByCategory() {
		Collections.sort(books, Books.bkCategoryComparator);
		for (Books bks : books) {
			System.out.println(bks.toString());
		}
	}

	private static void searchByBookId() {
		Scanner scan = new Scanner(System.in);
		System.out.print("Please enter Book ID: ");
		int bookId = scan.nextInt();
		int flag = 0;
		for (Books bks : books) {
			if (bks.getBookId() == bookId) {
				System.out.println(bks.toString());
				flag = 1;
			}
		}
		if (flag == 0) {
			System.out.println("No Book found by this ID!");
		}
	}

	private static void searchByAuthorName() {
		Scanner scan = new Scanner(System.in);
		System.out.print("Please enter Author Name: ");
		String authorName = scan.nextLine();
		int flag = 0;
		for (Books bks : books) {
			if (bks.getAuthorName().equalsIgnoreCase(authorName)) {
				System.out.println(bks.toString());
				flag = 1;
			}
		}
		if (flag == 0) {
			System.out.println("No Book found by this ID!");
		}
	}

	private static void searchByCategory() {
		Scanner scan = new Scanner(System.in);
		System.out.print("Please enter Category of the book: ");
		String category = scan.nextLine();
		int flag = 0;
		for (Books bks : books) {
			if (bks.getCategory().equalsIgnoreCase(category)) {
				System.out.println(bks.toString());
				flag = 1;
			}
		}
		if (flag == 0) {
			System.out.println("No Book found by this ID!");
		}
	}

	private static void modifyBooksData() {
		Scanner scan = new Scanner(System.in);
		System.out.print("Please enter book ID: ");
		int bookId = scan.nextInt();
		scan.nextLine();
		int flag = 0;
		for (Books bks : books) {
			if (bks.getBookId() == bookId) {
				System.out.println("Enter Book Name: ");
				String bookName = scan.nextLine();
				System.out.println("Enter Author Name: ");
				String authorName = scan.nextLine();
				System.out.println("Enter Book Category: ");
				String category = scan.nextLine();
				System.out.println("Enter Book Publisher: ");
				String publisher = scan.nextLine();
				System.out.println("Enter Book Price: ");
				double price = scan.nextDouble();
				scan.nextLine();
				bks.setBookName(bookName);
				bks.setAuthorName(authorName);
			bks.setCategory(category);
				bks.setPublisher(publisher);
				bks.setPrice(price);

				flag = 1;
			}
		}
		if (flag == 0) {
			System.out.println("No Book found by this ID!");
		}
	}

	private static void menu() {
		System.out.println("1. Display Book Details in Unsorted order " + "\n2.Display Book Details by Book Name "
				+ "\n3.Display Book Details by Author Name " + "\n4.Display Book Details by Category "
				+ "\n5.Search Book by Book ID " + "\n6.Search Book by Author Name " + "\n7.Search Book By Category "
				+ "\n8. Modify Book Data " + "\n9.Exit");
	}
}
